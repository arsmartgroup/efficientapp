﻿//------------------------------------------------------------------------------
// SqliteLoginDao.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/30/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------


namespace EfficientDao.Implements.Sqlites
{
    public class SqliteLoginDao : BaseDao, ILoginDao
    {
        public bool IsConfigured()
        {
            return false;
        }

        public bool IsLoggedIn()
        {
            return false;
        }
    }
}
