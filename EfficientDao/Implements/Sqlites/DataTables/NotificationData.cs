﻿//------------------------------------------------------------------------------
// NotificationData.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/22/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

using SQLite.Net.Attributes;

namespace EfficientDao.Implements.Sqlites.DataTables
{
    public class NotificationData : BaseData
    {
        [AutoIncrement, PrimaryKey]
        public long NotifictionId { get; set; }
        public string NotificationType { get; set; }
        public string Message { get; set; }
        public long RelatedId { get; set; }
        public bool IsRead { get; set; }
    }
}
