﻿//------------------------------------------------------------------------------
// QueryParams.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 07/03/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

using System.Collections.Generic;

namespace EfficientDao.Implements.WebApis.Helpers
{
    public class QueryParams
    {
        internal Page Page { get; set; }
        internal Dictionary<string, string> Filters { set; get; }
    }
}
