﻿//------------------------------------------------------------------------------
// WebApiHelper.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 07/03/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace EfficientDao.Implements.WebApis.Helpers
{
    internal class WebApiHelper
    {
        public static WebApiHelper Instance = new WebApiHelper();

        private WebApiHelper()
        {
        }

        public Action<HttpStatusCode> OnError { get; set; }

        public string ServerAddress
        {
            get
            {
                return string.Empty;
            }
            set
            {
            }
        }

        public string Token
        {
            get
            {
                return string.Empty;
            }
            set
            {
                
            }
        }

        public void BatchPost<T>(string uri, T data = default(T))
        {
            using (HttpClient client = CreateHttpClient())
            {
                HttpResponseMessage response = client.PostAsJsonAsync(string.Format("{0}/{1}", ServerAddress, uri), data).Result;
                if (!response.IsSuccessStatusCode)
                {
                    OnError(response.StatusCode);
                    throw new HttpRequestException(response.ReasonPhrase);
                }
            }
        }

        public async Task<HttpResponseMessage> Post<T>(string uri, T data = default(T))
        {
            using (HttpClient client = CreateHttpClient())
            {
                HttpResponseMessage response = await client.PostAsJsonAsync(string.Format("{0}/{1}", ServerAddress, uri), data);

                if (!response.IsSuccessStatusCode)
                {
                    OnError(response.StatusCode);
                    throw new HttpRequestException(response.ReasonPhrase);
                }

                return response;
            }
        }

        public async Task<HttpResponseMessage> Put<T>(string uri, T data)
        {
            using (HttpClient client = CreateHttpClient())
            {
                HttpResponseMessage response = await client.PutAsJsonAsync(string.Format("{0}/{1}", ServerAddress, uri), data);
                if (!response.IsSuccessStatusCode)
                {
                    OnError(response.StatusCode);
                    throw new HttpRequestException(response.ReasonPhrase);
                }

                return response;
            }
        }

        public async Task<T> GetAsync<T>(CancellationToken cancelToken, string uri, QueryParams queryParams = null)
        {
            using (HttpClient client = CreateHttpClient())
            {
                string queryString = string.Empty;
                if (queryParams != null)
                {
                    if (queryParams.Filters != null)
                    {
                        foreach (KeyValuePair<string, string> filter in queryParams.Filters)
                        {
                            if (!string.IsNullOrWhiteSpace(filter.Value))
                            {
                                queryString += string.Format("{0}={1}&", filter.Key, filter.Value);
                            }
                        }
                    }

                    if (queryParams.Page != null)
                    {
                        queryString += string.Format("PageNumber={0}&", queryParams.Page.PageNumber);
                        queryString += string.Format("PageSize={0}&", queryParams.Page.PageSize);
                        queryString += string.Format("SortBy={0}&", queryParams.Page.SortBy);
                        queryString += string.Format("IsDesc={0}&", queryParams.Page.IsDesc);
                    }
                }

                if (queryString.EndsWith("&"))
                {
                    queryString = queryString.Substring(0, queryString.Length - 1);
                }

                string requestUri = string.Format("{0}/{1}?{2}", ServerAddress, uri, queryString);
                if (requestUri.EndsWith("?"))
                {
                    requestUri = requestUri.Trim('?');
                }

                HttpResponseMessage response = await client.GetAsync(requestUri, cancelToken);

                if (!response.IsSuccessStatusCode)
                {
                    OnError(response.StatusCode);
                    throw new HttpRequestException(response.ReasonPhrase);
                }

                return await response.Content.ReadAsAsync<T>();
            }
        }

        public async Task<T> Get<T>(string uri, QueryParams queryParams = null)
        {
            using (HttpClient client = CreateHttpClient())
            {
                string queryString = string.Empty;
                if (queryParams != null)
                {
                    if (queryParams.Filters != null)
                    {
                        foreach (KeyValuePair<string, string> filter in queryParams.Filters)
                        {
                            if (!string.IsNullOrWhiteSpace(filter.Value))
                            {
                                queryString += string.Format("{0}={1}&", filter.Key, filter.Value);
                            }
                        }
                    }

                    if (queryParams.Page != null)
                    {
                        queryString += string.Format("PageNumber={0}&", queryParams.Page.PageNumber);
                        queryString += string.Format("PageSize={0}&", queryParams.Page.PageSize);
                        queryString += string.Format("SortBy={0}&", queryParams.Page.SortBy);
                        queryString += string.Format("IsDesc={0}&", queryParams.Page.IsDesc);
                    }
                }

                if (queryString.EndsWith("&"))
                {
                    queryString = queryString.Substring(0, queryString.Length - 1);
                }

                string requestUri = string.Format("{0}/{1}?{2}", ServerAddress, uri, queryString);
                if (requestUri.EndsWith("?"))
                {
                    requestUri = requestUri.Trim('?');
                }

                HttpResponseMessage response = await client.GetAsync(requestUri);

                if (!response.IsSuccessStatusCode)
                {
                    OnError(response.StatusCode);
                    throw new HttpRequestException(response.ReasonPhrase);
                }

                return await response.Content.ReadAsAsync<T>();
            }
        }

        public async Task<HttpResponseMessage> Delete(string uri)
        {
            using (HttpClient client = CreateHttpClient())
            {
                HttpResponseMessage response = await client.DeleteAsync(string.Format("{0}/{1}", ServerAddress, uri));
                if (!response.IsSuccessStatusCode)
                {
                    OnError(response.StatusCode);
                    throw new HttpRequestException(response.ReasonPhrase);
                }

                return response;
            }
        }

        public HttpClient CreateHttpClient()
        {
            HttpClient httpClient = new HttpClient();

            // add token if there is one
            if (!string.IsNullOrEmpty(Token))
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Token);
            }

            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.UserAgent.Add(new ProductInfoHeaderValue("accesstype", "mobileapp"));

            return httpClient;
        }
    }
}
