﻿//------------------------------------------------------------------------------
// Page.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 07/03/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

namespace EfficientDao.Implements.WebApis.Helpers
{
    public class Page
    {
        public bool HasNextPage { get; set; }

        public bool HasPreviousPage { get; set; }

        public int PageNumber { get; set; }

        public int PageSize { get; set; }

        public int TotalItems { get; set; }

        public int TotalPages { get; set; }

        public string SortBy { get; set; }

        public bool IsDesc { get; set; }
    }
}
