﻿//------------------------------------------------------------------------------
// WebApiDaoFactory.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/22/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

using EfficientDao.Implements.WebApis;

namespace EfficientDao.Implements.Factorys
{
    public class WebApiDaoFactory : IDaoFactory
    {
        public static WebApiDaoFactory Instance = new WebApiDaoFactory();

        private WebApiDaoFactory()
        {

        }

        public ILoginDao GetLoginDao()
        {
            return new WebApiLoginDao();
        }
    }
}
