﻿//------------------------------------------------------------------------------
// ILoginDao.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/22/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

using EfficientCommon;

namespace EfficientDao
{
    /// <summary>
    /// login dao interface.
    /// </summary>
    public interface ILoginDao
    {
        bool IsConfigured();

        bool IsLoggedIn();
    }
}
