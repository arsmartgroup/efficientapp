﻿//------------------------------------------------------------------------------
// ILog.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/30/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

using System;

namespace EfficientException
{
    public interface ILog
    {
        void WriteLog(string message);

        void WriteLog(Exception ex);

        void WriteLog(string message, Exception ex);
    }
}
