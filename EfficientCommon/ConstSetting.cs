﻿//------------------------------------------------------------------------------
// ConstSetting.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/21/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

namespace EfficientCommon
{
    public sealed class ConstSetting
    {
        //Color
        public const string NavigationPageBarBgColor = "#165c7d";
        public const string NavigationPageBarTextColor = "#FFFFFF";
        public const string ImgEntryBottomLineColor = "#DBDBDB";

        //
        public const int GetGeoLocatorCurrentPosTimeOut = 10000;

        //Image(contain extension)
        public const string SlideoutImageName = "slideout.png";
        public const string MenuLogoImageName = "menu_logo.png";
        public const string MenuItemDashboardImageName = "ic_menu_dashboard.png";

        //General parameter
        public const string SaveSettingsFailed = "SaveSettingsFailed";
        public const string SaveSettingsSucceed = "SaveSettingsSucceed";
        public const string LoginSucceed = "LoginSucceed";
        public const string LoginFailed = "LoginFailed";
        public const string IdSpliter = ",";
        public const string SQLiteFilename = "EfficientSQLite.db3";
        public const string GoogleApiNumber = "887270915280";
        public const int DefaultImageDimensionWidth = 1024;
        public const int DefaultImageDimensionHeight = 768;
        public const string DefaultUploadFolder = "Uploads\\Mobile";
        public const string EulaUrl = "https://www.baidu.com/EULA.htm";
        public const string XamarinInsightsKey = "909486bca5b55084383903515435ae86cf679075";
        public const string OfflinePrimaryKeyPostfix = "-o";
        public const string SortByDistance = "D";
        public const int DefaultMapRegionDistance = 4;

        //Formatter
        public const string DefaultDateFormatString = "yyyy-MM-dd";
        public const string DefaultTimeFormatString = "HH:mm:ss";
    }
}
