﻿//------------------------------------------------------------------------------
// DataTypes.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/22/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------


namespace EfficientCommon
{
    /// <summary>
    /// Database type.
    /// </summary>
    public enum DatabaseType
    {
        WebApi,
        SQLite
    }

    /// <summary>
    /// Page type
    /// </summary>
    public enum PageType : byte
    {
        Dashboard
    }

    /// <summary>
    /// Mobile scanner type
    /// </summary>
    public enum MobileScannerType : byte
    {
        BarCode,
        QRCode
    }

    /// <summary>
    /// Enum ScanModuleType
    /// </summary>
    public enum ScanModuleType : byte
    {
        Asset,
        Location,
        Part
    }

    public enum MenuType
    {
        Dashboard,
        ChangePassword,
        Setting,
        Logout
    }
}
