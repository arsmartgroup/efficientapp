﻿//------------------------------------------------------------------------------
// BaseModel.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/22/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;

using Xamarin.Forms;

namespace EfficientViewModels
{
    public class BaseModel : INotifyPropertyChanged
    {
        public event PropertyChangingEventHandler PropertyChanging;

        public event PropertyChangedEventHandler PropertyChanged;
        
        protected void SetProperty<T>(ref T propertyVar, T newValue, string propertyName)
        {
            if(string.IsNullOrEmpty(propertyName))
            {
                return;
            }

            if (EqualityComparer<T>.Default.Equals(propertyVar, newValue))
            {
                return;
            }

            OnPropertyChanging(propertyName);
            propertyVar = newValue;
            OnPropertyChanged(propertyName);
        }

        private void OnPropertyChanging(string propertyName = null)
        {
            PropertyChangingEventHandler handler = PropertyChanging;
            if (handler != null)
            {
                handler(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        private void OnPropertyChanged(string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
