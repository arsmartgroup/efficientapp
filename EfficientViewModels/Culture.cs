﻿//------------------------------------------------------------------------------
// Culture.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/22/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

namespace EfficientViewModels
{
    public class Culture : BaseModel
    {
        public byte CultureId { get; set; }
        public string TargetCode { get; set; }
        public string SourceCodes { get; set; }
        public string Name { get; set; }
        public bool IsEnabled { get; set; }
        public string DateFormat { get; set; }
        public string TimeFormat { get; set; }
        public string DateTimeFormat { get; set; }
        public string NameFormat { get; set; }
        public string WholeNumberFormat { get; set; }
        public string FractionNumberFormat { get; set; }
        public string CurrencyFormat { get; set; }
        public string PreciseCurrencyFormat { get; set; }
        public string EmptyText { get; set; }
        public bool RightToLeft { get; set; }
    }
}
