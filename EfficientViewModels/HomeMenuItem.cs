﻿//------------------------------------------------------------------------------
// HomeMenuItem.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/22/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

using EfficientCommon;

namespace EfficientViewModels
{
    public class HomeMenuItem : BaseModel
    {
        public HomeMenuItem()
        {
            MenuType = MenuType.Dashboard;
        }

        public long Id { get; set; }
        public string Title { get; set; }
        public string Icon { get; set; }
        public string IosIcon { get; set; }
        public MenuType MenuType { get; set; }
    }
}
