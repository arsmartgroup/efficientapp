﻿//------------------------------------------------------------------------------
// AppDelegate.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/22/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

using System;

using Foundation;
using PushNotification.Plugin;
using TK.CustomMap.iOSUnified;
using UIKit;
using Xamarin;
using Xamarin.Forms;
using XLabs.Forms;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Mvvm;
using XLabs.Platform.Services;
using XLabs.Platform.Services.Media;
using ZXing.Net.Mobile.Forms.iOS;

using EfficientApp.Tools.Helpers.Implements;

namespace EfficientApp.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : XFormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        private App _app;
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            SetIoc();

            Forms.Init();
            FormsMaps.Init();
            TKCustomMapRenderer.InitMapRenderer();
            NativePlacesApi.Init();

            ZXingScannerViewRenderer.Init();

            CrossPushNotification.Initialize<CrossPushNotificationListener>();

            _app = new App();
            LoadApplication(_app);

            return base.FinishedLaunching(app, options);
        }

        private void SetIoc()
        {
            if (!Resolver.IsSet)
            {
                SimpleContainer resolverContainer = new SimpleContainer();

                XFormsAppiOS app = new XFormsAppiOS();

                app.Init(this);

                resolverContainer.Register<IDevice>(t => AppleDevice.CurrentDevice)
                    .Register<IDisplay>(t => t.Resolve<IDevice>().Display)
                    .Register<INetwork>(t => t.Resolve<IDevice>().Network)
                    .Register<IMediaPicker, MediaPicker>()
                    .Register<IDependencyContainer>(resolverContainer)
                    .Register<IXFormsApp>(app);
                Resolver.SetResolver(resolverContainer.GetResolver());
            }
        }

        public override bool OpenUrl(UIApplication app, NSUrl url, string sourceApp, NSObject annotation)
        {
            if (url.ToString().IndexOf('?') > -1)
            {
                string query = url.ToString().Split('?')[1];
                _app.EfficientAppLinks(query);
            }

            return true;
        }

        public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
        {
            if (CrossPushNotification.Current is IPushNotificationHandler)
            {
                ((IPushNotificationHandler)CrossPushNotification.Current).OnErrorReceived(error);

            }
        }

        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            if (CrossPushNotification.Current is IPushNotificationHandler)
            {
                ((IPushNotificationHandler)CrossPushNotification.Current).OnRegisteredSuccess(deviceToken);
            }
        }

        public override void DidRegisterUserNotificationSettings(UIApplication application, UIUserNotificationSettings notificationSettings)
        {
            application.RegisterForRemoteNotifications();
        }

        public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
        {
            if (CrossPushNotification.Current is IPushNotificationHandler)
            {
                ((IPushNotificationHandler)CrossPushNotification.Current).OnMessageReceived(userInfo);
            }
        }

        public override void ReceivedRemoteNotification(UIApplication application, NSDictionary userInfo)
        {
            if (CrossPushNotification.Current is IPushNotificationHandler)
            {
                ((IPushNotificationHandler)CrossPushNotification.Current).OnMessageReceived(userInfo);
            }
        }
    }
}
