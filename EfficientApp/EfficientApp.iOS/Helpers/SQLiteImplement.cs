﻿//------------------------------------------------------------------------------
// SQLiteImplement.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 07/03/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

using System;
using System.IO;

using Xamarin.Forms;
using SQLite.Net;
using SQLite.Net.Platform.XamarinIOS;

using EfficientApp.Tools.Helpers;
using EfficientApp.iOS.Helpers;


[assembly: Dependency(typeof(SQLiteImplement))]
namespace EfficientApp.iOS.Helpers
{
    public class SQLiteImplement : ISQLite
    {
        public SQLiteConnection GetConnection(string sqliteFilename)
        {
            string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string path = Path.Combine(documentsPath, sqliteFilename);
            SQLiteConnection conn = new SQLiteConnection(new SQLitePlatformIOS(), path);
            return conn;
        }
    }
}
