//------------------------------------------------------------------------------
// SplashScreen.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/21/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Content.Res;
using Android.OS;

namespace EfficientApp.Droid
{
    [Activity(MainLauncher = true, NoHistory = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    [IntentFilter(new[] { Intent.ActionView }, DataScheme = "EfficientApp", Categories = new[] { Intent.CategoryDefault, Intent.CategoryBrowsable })]
    public class SplashScreen : Activity
    {
        private readonly Handler _hander = new Handler();

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            var orientaiont = Resources.Configuration.Orientation;
            SetTheme(orientaiont == Orientation.Landscape
                ? Resource.Style.Theme_Landscape_Splash
                : Resource.Style.Theme_Splash);

            _hander.PostDelayed(() =>
            {
                var intent = new Intent(this, typeof(MainActivity));
                intent.SetData(Intent.Data);
                StartActivity(intent);
                Finish();
            }, 500);
        }
    }
}