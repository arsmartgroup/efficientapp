﻿//------------------------------------------------------------------------------
// MainActivity.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/22/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------


using Android.App;
using Android.Content.PM;
using Android.OS;

using Acr.UserDialogs;
using Xamarin;
using Xamarin.Forms;
using PushNotification.Plugin;
using XLabs.Forms;
using XLabs.Platform.Mvvm;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services.Media;

using EfficientApp.Tools.Helpers.Implements;
using EfficientCommon;

namespace EfficientApp.Droid
{
    [Activity(Label = "EfficientApp",Theme = "@style/Theme.MainTheme", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : XFormsApplicationDroid
    {
        private App _currentApp;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            UserDialogs.Init(() => (Activity)Forms.Context);

            if (!Resolver.IsSet)
            {
                SetIoc();
            }
            else
            {
                IXFormsApp<XFormsApplicationDroid> app = Resolver.Resolve<IXFormsApp>() as IXFormsApp<XFormsApplicationDroid>;
                if (app != null)
                {
                    app.AppContext = this;
                }
            }

            Forms.Init(this, bundle);
            FormsMaps.Init(this, bundle);

            CrossPushNotification.Initialize<CrossPushNotificationListener>(ConstSetting.GoogleApiNumber);

            _currentApp = new App();
            if (Intent.Data != null)
            {
                _currentApp.EfficientAppLinks(Intent.Data.EncodedQuery);
            }

            LoadApplication(new App());
        }

        private void SetIoc()
        {
            SimpleContainer resolverContainer = new SimpleContainer();

            XFormsAppDroid app = new XFormsAppDroid();

            app.Init(this);

            resolverContainer.Register(t => AndroidDevice.CurrentDevice)
                .Register(t => t.Resolve<IDevice>().Display)
                .Register(t => t.Resolve<IDevice>().Network)
                .Register<IMediaPicker, MediaPicker>()
                .Register<IDependencyContainer>(resolverContainer)
                .Register<IXFormsApp>(app);

            Resolver.SetResolver(resolverContainer.GetResolver());
        }
    }
}

