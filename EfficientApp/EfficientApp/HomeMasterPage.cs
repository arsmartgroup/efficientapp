﻿//------------------------------------------------------------------------------
// HomeMasterPage.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/22/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

using System;

using Xamarin.Forms;

using EfficientApp.Areas;
using EfficientApp.Areas.Dashboard.Views;
using EfficientApp.Areas.Logins.Views;
using EfficientApp.CustomCtrls;
using EfficientBusiness;
using EfficientCommon;
using EfficientViewModels;


namespace EfficientApp
{
    public class HomeMasterPage : BasePage
    {
        private Page _pageSelection;
        private MenuType _menuType = MenuType.Dashboard;
        private LoginLogic _loginLogic = new LoginLogic();
        private Page _dashboard;

        public Action<MenuType> PageSelectionChanged;

        public Page PageSelection
        {
            get { return _pageSelection; }
            set
            {
                _pageSelection = value;
                if (PageSelectionChanged != null)
                {
                    PageSelectionChanged(_menuType);
                }
            }
        }

        public HomeMasterPage(HomeViewModel viewModel)
        {
            Icon = Device.RuntimePlatform == Device.iOS ? ConstSetting.SlideoutImageName.Split('.')[0] : ConstSetting.SlideoutImageName;
            BindingContext = viewModel;

            StackLayout rootLayout = new StackLayout
            {
                Spacing = 0,
                BackgroundColor = Color.FromHex(ConstSetting.NavigationPageBarBgColor)
            };

            if (Device.RuntimePlatform == Device.iOS)
            {
                rootLayout.Padding = new Thickness(0, 22, 0, 0);
            }

            ListView menu = new ListView
            {
                RowHeight = 50,
                BackgroundColor = Color.FromHex(ConstSetting.NavigationPageBarBgColor),
                SeparatorColor = Color.FromHex(ConstSetting.NavigationPageBarBgColor),
                SeparatorVisibility = SeparatorVisibility.Default,
            };

            menu.ItemTemplate = new DataTemplate(typeof(MenuImageCell));
            menu.ItemsSource = viewModel.MenuItems;
            menu.ItemSelected += Menu_ItemSelected;
            menu.ItemTapped += Menu_ItemTapped;

            if (!_loginLogic.IsConfigured() || !_loginLogic.IsLoggedIn())
            {
                PageSelection = new NavigationPage(new LoginPage())
                {
                    BarBackgroundColor = Color.FromHex(ConstSetting.NavigationPageBarBgColor),
                    BarTextColor = Color.FromHex(ConstSetting.NavigationPageBarTextColor)
                };
            }
            else if (App.IsOffline)
            {

            }
            else
            {
                if (_dashboard == null)
                {
                    _dashboard = new DashboardPage();
                }

                PageSelection = _dashboard;
            }

            StackLayout menuLogoLayout = new StackLayout
            {
                Spacing = 1,
                BackgroundColor = Color.FromHex(ConstSetting.NavigationPageBarBgColor),
                Padding = new Thickness(0, 20, 0, 10)
            };

            Image logo = new Image
            {
                Source = ImageSource.FromFile(ConstSetting.MenuLogoImageName),
                Aspect = Aspect.AspectFit,
            };

            switch (Device.RuntimePlatform)
            {
                case Device.WinPhone:
                    logo = new Image
                    {
                        Source = ImageSource.FromFile(string.Format("Images/{0}", ConstSetting.MenuLogoImageName)),
                        Aspect = Aspect.AspectFit,
                    };
                    break;
            }

            menuLogoLayout.Children.Add(logo);

            rootLayout.Children.Add(menuLogoLayout);
            rootLayout.Children.Add(menu);

            Content = rootLayout;
        }

        private void Menu_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            ListView menu = sender as ListView;
            if (menu == null)
            {
                return;
            }

            menu.SelectedItem = null;
        }

        private void Menu_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ListView menu = sender as ListView;
            if (menu == null)
            {
                return;
            }

            App.IsSyncingData = false;
            var menuItem = menu.SelectedItem as HomeMenuItem;
            if (menuItem != null)
            {
                //if (App.IsOffline && menuItem.MenuType != MenuType.WorkOrders)
                //{
                //    UserDialogs.Instance.Alert(Text.Prompt_Disconnected);
                //    return;
                //}

                _menuType = menuItem.MenuType;
                if (_loginLogic.IsConfigured() && _loginLogic.IsLoggedIn())
                {
                    switch (menuItem.MenuType)
                    {
                        case MenuType.Dashboard:
                            if (_dashboard == null)
                            {
                                _dashboard = new DashboardPage();
                            }

                            PageSelection = _dashboard;
                            break;
                    }
                }
                else
                {
                    PageSelection = new LoginPage();
                }
            }
        }

    }
}
