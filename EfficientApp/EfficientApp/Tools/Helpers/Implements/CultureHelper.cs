﻿//------------------------------------------------------------------------------
// CultureHelper.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/22/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;

using EfficientViewModels;


namespace EfficientApp.Tools.Helpers.Implements
{
    public class CultureHelper
    {
        private static readonly CultureHelper _instance = new CultureHelper();
        private List<Culture> _cultures;
        private Culture _currentCulture;

        private CultureHelper()
        {

        }

        public static CultureHelper Instance
        {
            get { return _instance; }
        }

        public List<Culture> Cultures
        {
            set
            {
                _cultures = value;
                //_commonService.SaveCultures(value.Select(x =>
                //    new CultureData { CultureId = x.CultureId, TargetCode = x.TargetCode, Value = JsonConvert.SerializeObject(x) }));
            }
            get
            {
                return _cultures;
            }
        }

        public Culture CurrentCulture
        {
            get
            {
                Localize localize = new Localize();
                _currentCulture = _cultures.FirstOrDefault(x => x.TargetCode == localize.CurrentCulture.Name) ??
                        _cultures.FirstOrDefault(x => x.TargetCode == "en-US");

                return _currentCulture;
            }
        }

        public bool HasValue
        {
            get { return _cultures != null && _cultures.Count == 0; }
        }
    }
}
