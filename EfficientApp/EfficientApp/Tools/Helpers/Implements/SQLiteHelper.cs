﻿//------------------------------------------------------------------------------
// SQLiteHelper.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/22/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

using EfficientCommon;
using SQLite.Net;
using Xamarin.Forms;

namespace EfficientApp.Tools.Helpers.Implements
{
    public class SQLiteHelper
    {
        public static SQLiteHelper Instance = new SQLiteHelper();
        public readonly SQLiteConnection SqLiteConnection;

        private SQLiteHelper()
        {
            SqLiteConnection = DependencyService.Get<ISQLite>().GetConnection(ConstSetting.SQLiteFilename);
        }
    }
}
