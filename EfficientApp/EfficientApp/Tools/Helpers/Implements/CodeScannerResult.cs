﻿//------------------------------------------------------------------------------
// CodeScannerResult.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/22/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

using System.Collections.Generic;

using ZXing;

using EfficientCommon;

namespace EfficientApp.Tools.Helpers.Implements
{
    public sealed class CodeScannerResult
    {
        private List<string> _errors = new List<string>();

        public Result OriginalResult { get; set; }

        public MobileScannerType ScannerType { get; set; }

        public ScanModuleType ModuleType { get; set; }

        public object Value { get; set; }

        public List<string> Errors
        {
            get { return _errors; }
        }
    }
}
