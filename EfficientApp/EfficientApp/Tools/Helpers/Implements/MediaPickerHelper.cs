﻿//------------------------------------------------------------------------------
// MediaPickerHelper.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/22/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

using System;
using System.Threading.Tasks;

using Xamarin.Forms;
using Acr.UserDialogs;
using Plugin.Media;
using Plugin.Media.Abstractions;

using EfficientApp.Resources;


namespace EfficientApp.Tools.Helpers.Implements
{
    public class MediaPickerHelper
    {
        public static MediaPickerHelper Instance = new MediaPickerHelper();
        private static readonly IMediaPickerHelper _mediaPicker;

        static MediaPickerHelper()
        {
            _mediaPicker = DependencyService.Get<IMediaPickerHelper>();
        }

        /// <summary>
        /// Takes the picture.
        /// </summary>
        /// <returns>Task&lt;MediaFile&gt;.</returns>
        public async Task<MediaFile> TakePicture()
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                UserDialogs.Instance.Alert(Text.Error_Failure);
                return null;
            }

            if (_mediaPicker != null
                && !await _mediaPicker.GetCameraPermissionAsync())
            {
                return null;
            }

            return await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
            {
                DefaultCamera = CameraDevice.Rear,
                Name = string.Format(Text.Format_Underline, Text.IMG, DateTime.Now.ToString("ddyyyymm_hhmmss"))
            });
        }

        /// <summary>
        /// Selects the picture.
        /// </summary>
        /// <returns>Task&lt;MediaFile&gt;.</returns>
        public async Task<MediaFile> SelectPicture()
        {
            await CrossMedia.Current.Initialize();

            try
            {
                return await CrossMedia.Current.PickPhotoAsync();
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.Alert(ex.Message);
            }

            return null;
        }
    }
}
