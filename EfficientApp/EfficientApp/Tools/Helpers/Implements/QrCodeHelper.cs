﻿//------------------------------------------------------------------------------
// QrCodeHelper.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/22/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

using System.Threading.Tasks;

using Acr.UserDialogs;
using ZXing;
using ZXing.Mobile;

using EfficientApp.Resources;
using EfficientCommon;

namespace EfficientApp.Tools.Helpers.Implements
{
    public class QrCodeHelper
    {
        public static QrCodeHelper Instance = new QrCodeHelper();

        private QrCodeHelper() { }

        /// <summary>
        /// scanner code
        /// </summary>
        /// <returns></returns>
        public async Task<CodeScannerResult> Scanner()
        {
            MobileBarcodeScanner scanner = new MobileBarcodeScanner();
            Result result = await scanner.Scan();
            if (result == null)
            {
                return null;
            }

            return RecognitionCode(result);
        }

        /// <summary>
        /// Do Handle
        /// </summary>
        /// <param name="result"></param>
        /// <param name="pageType"></param>
        public void DoHandle(Result result, PageType pageType = PageType.Dashboard)
        {
            if (result == null)
            {
                return;
            }

            switch (result.BarcodeFormat)
            {
                case BarcodeFormat.QR_CODE:
                    if (result.Text.IndexOf('?') > -1)
                    {
                        string query = result.Text.Split('?')[1];
                        ShowAction(query, pageType, MobileScannerType.QRCode);
                    }
                    else
                    {
                        throw new System.Exception(Text.Error_UnsupportedCode);
                    }
                    break;

                default:
                    ShowModuleType(result.Text, pageType, MobileScannerType.BarCode);
                    break;
            }
        }

        /// <summary>
        /// Shows the action.
        /// </summary>
        /// <param name="query">The query.</param>
        public void ShowAction(string query,
            PageType pageType = PageType.Dashboard,
            MobileScannerType scannerType = MobileScannerType.QRCode)
        {
            if (QueryVerification(query))
            {
                ActionSheet(GetValueByQueryString(query, "m"), GetValueByQueryString(query, "i"), pageType, scannerType);
            }
            else
            {
                UserDialogs.Instance.Alert(Text.Error_UnsupportedCode);
            }
        }

        private CodeScannerResult RecognitionCode(Result result)
        {
            CodeScannerResult scannerResult = new CodeScannerResult();
            scannerResult.OriginalResult = result;

            switch (result.BarcodeFormat)
            {
                case BarcodeFormat.QR_CODE:
                    scannerResult.ScannerType = MobileScannerType.QRCode;

                    string query = string.Empty;
                    if (result.Text.IndexOf('?') > -1)
                    {
                        query = result.Text.Split('?')[1];
                    }
                    else
                    {
                        scannerResult.Errors.Add(Text.Error_UnsupportedCode);
                        break;
                    }

                    if (QueryVerification(query))
                    {
                        string moduleType = GetValueByQueryString(query, "m").ToLower();
                        switch (moduleType)
                        {
                            case "asset":
                                scannerResult.ModuleType = ScanModuleType.Asset;
                                break;

                            case "location":
                                scannerResult.ModuleType = ScanModuleType.Location;
                                break;

                            case "part":
                                scannerResult.ModuleType = ScanModuleType.Part;
                                break;
                        }

                        if (!string.IsNullOrEmpty(moduleType))
                        {
                            scannerResult.Value = GetValueByQueryString(query, "i");
                        }
                    }
                    break;

                default:
                    scannerResult.ScannerType = MobileScannerType.BarCode;
                    scannerResult.Value = result.Text;
                    break;
            }

            return scannerResult;
        }

        private string GetValueByQueryString(string query, string key)
        {
            string returnValue = "";
            if (query != "" && key != "" && query.IndexOf('=') > -1)
            {
                string[] queryArray = query.Split('&');

                for (int i = 0, length = queryArray.Length; i < length; i++)
                {
                    if (queryArray[i].IndexOf('=') > -1)
                    {
                        if (queryArray[i].Split('=')[0].ToLower() == key.ToLower())
                        {
                            returnValue = queryArray[i].Split('=')[1];
                            break;
                        }
                    }
                }
            }

            return returnValue;
        }

        private bool QueryVerification(string query)
        {
            bool isQuery = false;


            return isQuery;
        }

        private void ShowModuleType(string scanResults,
            PageType pageType = PageType.Dashboard,
            MobileScannerType scannerType = MobileScannerType.QRCode)
        {
            ActionSheetConfig cfg = new ActionSheetConfig();
            cfg.SetTitle(Text.Prompt_Scan);
            cfg.SetCancel(Text.Cancel);


            UserDialogs.Instance.ActionSheet(cfg);
        }

        private async void ActionSheet(string module,
            string id,
            PageType pageType = PageType.Dashboard,
            MobileScannerType scannerType = MobileScannerType.QRCode)
        {
            ActionSheetConfig cfg = new ActionSheetConfig();
            cfg.SetCancel(Text.Cancel);

            UserDialogs.Instance.ActionSheet(cfg);

        }
    }
}
