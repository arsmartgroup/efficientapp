﻿//------------------------------------------------------------------------------
// Localize.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/22/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

using System.Globalization;
using System.Reflection;
using System.Resources;

using Xamarin.Forms;

namespace EfficientApp.Tools.Helpers.Implements
{
    public class Localize
    {
        private static readonly ILocalize _localize;

        static Localize()
        {
            _localize = DependencyService.Get<ILocalize>();
        }

        public CultureInfo CurrentCulture
        {
            get { return _localize.GetCurrentCultureInfo(); }
        }

        /// <summary>
        /// Sets the localize.
        /// </summary>
        /// <param name="localize">The localize.</param>
        public void SetLocalize(string localize)
        {
            _localize.SetLocale(localize);
        }

        /// <summary>
        /// Gets the string.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="comment">The comment.</param>
        /// <returns>System.String.</returns>
        public static string GetString(string key, string comment)
        {
            ResourceManager temp = new ResourceManager("EfficientApp.Resources.Text", typeof(Localize).GetTypeInfo().Assembly);

            string result = temp.GetString(key, _localize.GetCurrentCultureInfo());

            return result;
        }
    }
}
