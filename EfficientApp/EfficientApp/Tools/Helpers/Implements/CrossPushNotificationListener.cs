﻿//------------------------------------------------------------------------------
// CrossPushNotificationListener.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/22/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

using PushNotification.Plugin;
using PushNotification.Plugin.Abstractions;
using Newtonsoft.Json.Linq;

namespace EfficientApp.Tools.Helpers.Implements
{
    public class CrossPushNotificationListener : IPushNotificationListener
    {
        public void OnError(string message, DeviceType deviceType)
        {

        }

        public void OnMessage(JObject values, DeviceType deviceType)
        {

        }

        public void OnRegistered(string token, DeviceType deviceType)
        {

        }

        public void OnUnregistered(DeviceType deviceType)
        {

        }

        public bool ShouldShowNotification()
        {
            return true;
        }
    }
}
