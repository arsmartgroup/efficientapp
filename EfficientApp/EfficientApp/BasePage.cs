﻿//------------------------------------------------------------------------------
// BasePage.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/22/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

using Xamarin.Forms;

using EfficientApp.Areas;

namespace EfficientApp
{
    /// <summary>
    /// Base page class.
    /// </summary>
    public class BasePage : ContentPage
    {
        public BasePage()
        {
            SetBinding(TitleProperty, new Binding(BaseViewModel.TitlePropertyName));
            SetBinding(IconProperty, new Binding(BaseViewModel.IconPropertyName));

        }
    }
}
