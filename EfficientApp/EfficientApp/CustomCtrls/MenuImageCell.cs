﻿//------------------------------------------------------------------------------
// MenuImageCell.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/22/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

using Xamarin.Forms;

namespace EfficientApp.CustomCtrls
{
    public class MenuImageCell : ViewCell
    {
        public MenuImageCell()
        {
            Image icon = new Image
            {
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.Center
            };

            icon.SetBinding(Image.SourceProperty, Device.RuntimePlatform == Device.iOS ? new Binding("IosIcon") : new Binding("Icon"));

            Label title = new Label
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.Center,
                FontSize = 16,
                TextColor = Color.White
            };
            title.SetBinding(Label.TextProperty, "Title");

            Grid layout = new Grid
            {
                Padding = 10
            };

            layout.ColumnDefinitions = new ColumnDefinitionCollection
            {
                new ColumnDefinition { Width = 20 },
                new ColumnDefinition { Width = 2 },
                new ColumnDefinition()
            };

            layout.Children.Add(icon);
            layout.Children.Add(title);

            Grid.SetRow(icon, 0);
            Grid.SetColumn(icon, 0);

            Grid.SetRow(title, 0);
            Grid.SetColumn(title, 2);

            View = layout;
        }
    }
}
