﻿//------------------------------------------------------------------------------
// HomePage.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/22/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

using System.Collections.Generic;

using Xamarin.Forms;

using EfficientApp.Areas;
using EfficientApp.Resources;
using EfficientCommon;


namespace EfficientApp
{
    public class HomePage : MasterDetailPage
    {
        private readonly HomeMasterPage _master;
        private Dictionary<MenuType, NavigationPage> _pages;
        private readonly Color _navigationPageBarBgColor;
        private readonly Color _navigationPageBarTextColor;

        public Dictionary<MenuType, NavigationPage> Pages
        {
            get { return _pages; }
            set { _pages = value; }
        }

        public HomePage()
        {
            HomeViewModel viewModel = new HomeViewModel();

            _pages = new Dictionary<MenuType, NavigationPage>();
            _navigationPageBarBgColor = Color.FromHex(ConstSetting.NavigationPageBarBgColor);
            _navigationPageBarTextColor = Color.FromHex(ConstSetting.NavigationPageBarTextColor);
            _master = new HomeMasterPage(viewModel);

            BindingContext = viewModel;
            Master = _master;

            NavigationPage homeNav = new NavigationPage(_master.PageSelection)
            {
                BarBackgroundColor = _navigationPageBarBgColor,
                BarTextColor = _navigationPageBarTextColor
            };

            Detail = homeNav;
            Detail.Title = Text.Dashboard;

            _pages.Add(MenuType.Dashboard, homeNav);
            _master.PageSelectionChanged = OnPageSelectionChanged;

            Icon = Device.RuntimePlatform == Device.iOS ? ConstSetting.SlideoutImageName.Split('.')[0] : ConstSetting.SlideoutImageName;
        }

        private async void OnPageSelectionChanged(MenuType menuType)
        {
            if (Detail != null && Device.RuntimePlatform == Device.WinPhone)
            {
                await Detail.Navigation.PopToRootAsync();
            }

            NavigationPage newPage;
            if (_pages.ContainsKey(menuType))
            {
                newPage = _pages[menuType];
            }
            else
            {
                newPage = new NavigationPage(_master.PageSelection)
                {
                    BarBackgroundColor = _navigationPageBarBgColor,
                    BarTextColor = _navigationPageBarTextColor
                };

                _pages.Add(menuType, newPage);
            }

            IsPresented = false;
            Detail = newPage;
            Detail.Title = _master.PageSelection.Title;
            if (Device.Idiom != TargetIdiom.Tablet)
            {
                IsPresented = false;
            }
        }
    }
}
