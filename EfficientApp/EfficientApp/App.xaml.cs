﻿//------------------------------------------------------------------------------
// App.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/22/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

using Xamarin.Forms;
using XLabs.Ioc;
using XLabs.Platform.Mvvm;
using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using PushNotification.Plugin;

using EfficientCommon;


namespace EfficientApp
{
    public partial class App : Application
    {
        private static IGeolocator _currentGeoLocator;

        public App()
        {
            Init();
            InitializeComponent();

            //MainPage = new ContentPage
            //{
            //    Content = new Label
            //    {
            //        Text = "Welcome to Xamarin Forms!"
            //    }
            //};

            MainPage = new HomePage();

            Navigation = MainPage.Navigation;

            CurrentGeoLocator.GetPositionAsync(ConstSetting.GetGeoLocatorCurrentPosTimeOut).ContinueWith(x =>
            {
                CurrentPosition = x.Result;
            });
        }

        public static INavigation Navigation;

        public static IGeolocator CurrentGeoLocator
        {
            get
            {
                if (_currentGeoLocator == null)
                {
                    _currentGeoLocator = CrossGeolocator.Current;
                    _currentGeoLocator.AllowsBackgroundUpdates = false;
                }
                return _currentGeoLocator;
            }
        }

        public static Position CurrentPosition { get; set; }

        public static bool IsOffline { get; set; }

        public static bool IsSyncingData { get; set; }

        public static bool IsLoadOfflineData
        {
            get { return IsOffline || IsSyncingData; }
        }

        public static void Init()
        {
            IXFormsApp app = Resolver.Resolve<IXFormsApp>();
            if (app == null)
            {
                return;
            }

            IsOffline = !CrossConnectivity.Current.IsConnected;

            CrossConnectivity.Current.ConnectivityChanged -= Current_ConnectivityChanged;
            CrossConnectivity.Current.ConnectivityChanged += Current_ConnectivityChanged;
        }

        private static void Current_ConnectivityChanged(object sender, ConnectivityChangedEventArgs e)
        {
            IsOffline = !e.IsConnected;

        }

        protected override void OnStart()
        {
            base.OnStart();
            CrossPushNotification.Current.Register();
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        public void EfficientAppLinks(string query)
        {

        }
    }
}
