﻿//------------------------------------------------------------------------------
// HomeViewModel.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/22/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

using System.Collections.ObjectModel;

using EfficientApp.Resources;
using EfficientCommon;
using EfficientViewModels;

namespace EfficientApp.Areas
{
    public class HomeViewModel : BaseViewModel
    {
        public ObservableCollection<HomeMenuItem> MenuItems { get; set; }

        public HomeViewModel()
        {
            CanLoadMore = true;
            Title = Text.MainMenu;
            SetMainMenuItems();
        }

        private void SetMainMenuItems()
        {
            MenuItems = new ObservableCollection<HomeMenuItem>
                {
                    new HomeMenuItem
                    {
                        Id = 2,
                        Title = Text.Dashboard,
                        MenuType = MenuType.Dashboard,
                        Icon = ConstSetting.MenuItemDashboardImageName,
                        IosIcon = ConstSetting.MenuItemDashboardImageName.Split('.')[0]
                    }
                };
        }
    }
}
