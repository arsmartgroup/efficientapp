﻿//------------------------------------------------------------------------------
// BaseViewModel.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/22/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

using System;
using System.Threading.Tasks;

using Acr.UserDialogs;
using Xamarin.Forms;
using ZXing.Mobile;

using EfficientApp.Tools.Helpers.Implements;
using EfficientCommon;
using EfficientViewModels;

namespace EfficientApp.Areas
{
    public class BaseViewModel : BaseModel
    {
        private string _title = string.Empty;
        private string _subTitle = string.Empty;
        private string _icon;
        private bool _isBusy;
        private bool _canLoadMore = true;
        private const string CanLoadMorePropertyName = "CanLoadMore";
        private const string IsBusyPropertyName = "IsBusy";
        public const string IconPropertyName = "Icon";
        public const string SubTitlePropertyName = "SubTitle";
        public const string TitlePropertyName = "Title";
        private Command _toolbarItemScanQrCodeCommand;
        private Command _toolbarItemUploadPictureCommand;

        public bool IsFirstLoad { get; set; }

        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value, TitlePropertyName); }
        }

        public string SubTitle
        {
            get { return _subTitle; }
            set { SetProperty(ref _subTitle, value, SubTitlePropertyName); }
        }

        public string Icon
        {
            get { return _icon; }
            set { SetProperty(ref _icon, value, IconPropertyName); }
        }

        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value, IsBusyPropertyName); }
        }

        public bool CanLoadMore
        {
            get { return _canLoadMore; }
            set { SetProperty(ref _canLoadMore, value, CanLoadMorePropertyName); }
        }

        public Command ToolbarItemScanQrCodeCommand
        {
            get
            {
                return _toolbarItemScanQrCodeCommand ??
                       (_toolbarItemScanQrCodeCommand = new Command(async () => await ExecuteToolbarItemScanQrCodeCommand()));
            }
        }

        public Command ToolbarItemUploadPictureCommand
        {
            get
            {
                return _toolbarItemUploadPictureCommand;
                //return _toolbarItemUploadPictureCommand ??
                //       (_toolbarItemUploadPictureCommand =
                //           new Command(
                //               async () =>
                //               {
                //                   if (App.IsOffline)
                //                   {
                //                       UserDialogs.Instance.Toast(Text.Prompt_OfflineMode, new TimeSpan(0, 0, 5));
                //                       return;
                //                   }

                //                   try
                //                   {
                //                       byte[] uploadedImage = null;
                //                       string filePath = string.Empty;
                //                       MediaFile mediaFile = null;
                //                       MediaPickerHelper mediaPicker = MediaPickerHelper.Instance;
                //                       string result = await
                //                           UserDialogs.Instance.ActionSheetAsync(Text.UploadPicture, Text.Cancel,
                //                               null, null, Text.TakePicture, Text.SelectPicture);
                //                       if (result == Text.TakePicture)
                //                       {
                //                           mediaFile = await mediaPicker.TakePicture();
                //                       }
                //                       else if (result == Text.SelectPicture)
                //                       {
                //                           mediaFile = await mediaPicker.SelectPicture();
                //                       }

                //                       if (mediaFile != null)
                //                       {
                //                           filePath = mediaFile.Path;
                //                           using (MemoryStream stream = new MemoryStream())
                //                           {
                //                               mediaFile.GetStream().CopyTo(stream);
                //                               uploadedImage = stream.ToArray();
                //                           }


                //                           ActionSheetConfig cfg = new ActionSheetConfig();
                //                           cfg.SetTitle(Text.Create);
                //                           cfg.SetCancel(Text.Cancel);


                //                           UserDialogs.Instance.ActionSheet(cfg);
                //                       }
                //                   }
                //                   catch (Exception ex)
                //                   {
                //                       UserDialogs.Instance.Alert(ex.Message);
                //                   }
                //               }));
            }
        }

        protected virtual PageType GetPageType()
        {
            return PageType.Dashboard;
        }

        private async Task ExecuteToolbarItemScanQrCodeCommand()
        {
            try
            {
                QrCodeHelper.Instance.DoHandle(await new MobileBarcodeScanner().Scan(), GetPageType());
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.Alert(ex.Message);
            }
        }
    }
}
