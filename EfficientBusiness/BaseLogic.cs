﻿//------------------------------------------------------------------------------
// BaseLogic.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/22/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------

using System;

using EfficientCommon;
using EfficientDao;
using EfficientDao.Implements.Factorys;
using EfficientException;


namespace EfficientBusiness
{
    public class BaseLogic : ILog
    {
        protected IDaoFactory FactoryInstance = WebApiDaoFactory.Instance;
        
        protected void ResetDaoFactory(DatabaseType dbType)
        {
            switch (dbType)
            {
                case DatabaseType.SQLite:
                    FactoryInstance = SqliteDaoFactory.Instance;
                    break;

                case DatabaseType.WebApi:
                    FactoryInstance = WebApiDaoFactory.Instance;
                    break;
            }
        }

        #region Write Log
        public void WriteLog(Exception ex)
        {
            new BaseException(ex.Message, ex);
        }

        public void WriteLog(string message)
        {
            new BaseException(message, null);
        }

        public void WriteLog(string message, Exception ex)
        {
            new BaseException(message, ex);
        }
        #endregion
    }
}
