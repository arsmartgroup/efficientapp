﻿//------------------------------------------------------------------------------
// LoginLogic.cs
//
// <copyright from='2005' to='2015' company='Smartware Enterprises Inc'> 
// Copyright (c) Smartware Enterprises Inc. All Rights Reserved. 
// Information Contained Herein is Proprietary and Confidential. 
// </copyright>
//
// Created: 06/22/2017
// Owner: Chonglong Bai
//
//------------------------------------------------------------------------------


using EfficientDao;

namespace EfficientBusiness
{
    public sealed class LoginLogic : BaseLogic
    {
        private ILoginDao _loginDao;

        public LoginLogic()
        {
            _loginDao = FactoryInstance.GetLoginDao();
        }

        public bool IsConfigured()
        {
            return _loginDao.IsConfigured();
        }

        public bool IsLoggedIn()
        {
            return _loginDao.IsLoggedIn();
        }
    }
}
